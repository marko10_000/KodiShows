# SPDX-License-Identifier: AGPL-3.0
import os


def updateFile(file:str, content:bytes):
	assert isinstance(file, str)
	assert isinstance(content, bytes)
	valid:bool
	if os.path.isfile(file):
		with open(file, "rb") as f:
			valid = f.read() == content
	else:
		valid = False
	if not valid:
		with open(file, "wb") as f:
			f.write(content)
