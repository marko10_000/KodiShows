# SPDX-License-Identifier: AGPL-3.0
import itertools
import os
import re
import typing


_numbers = set("0123456789.")


class Group:
	pre:str
	post:str

	def __init__(self, pre:str, post:str):
		assert isinstance(pre, str)
		assert isinstance(post, str)
		self.pre = pre.lower()
		self.post = post.lower()

	def match(self, name:typing.Union[str, "File"]) -> bool:
		assert isinstance(name, (str, File))
		tmp:str
		if isinstance(name, str):
			tmp = name
		elif isinstance(name, File):
			tmp = name.internal
		else:
			raise ValueError()
		tmp_prefix = tmp[:min(len(self.pre), len(tmp))].lower()
		tmp_postfix = tmp[max(0, len(tmp) - len(self.post)):].lower()
		return (self.pre == tmp_prefix) and (self.post == tmp_postfix)

	def getName(self, name:typing.Union[str, "File"]) -> str:
		assert isinstance(name, (str, File))
		tmp:str
		if isinstance(name, str):
			tmp = name
		elif isinstance(name, File):
			tmp = name.internal
		else:
			raise ValueError()
		assert self.match(tmp)
		return tmp[len(self.pre):(None if not self.post else -len(self.post))]

	@staticmethod
	def makeGroupe(a:str, b:str) -> "Group":
		assert isinstance(a, str)
		assert isinstance(b, str)
		ab_max = min(len(a), len(b))
		tmp_a = a.lower()
		tmp_b = b.lower()

		# find pre
		counter = 0
		for _ in range(ab_max):
			if tmp_a[counter] != tmp_b[counter]:
				break
			counter += 1
		pre = a[:counter]

		# Fix pre numbers
		for i in range(len(pre))[::-1]:
			if pre[i] in _numbers:
				pre = pre[:-1]
			else:
				break

		# find post
		counter = 0
		for _ in range(ab_max):
			if tmp_a[len(a) - counter - 1] != tmp_b[len(b) - counter - 1]:
				break
			counter += 1
		post = a[len(a) - counter:]

		# Fis post numbers
		for i in range(len(post)):
			if post[0] in _numbers:
				post = post[1:]
			else:
				break

		return Group(pre, post)

	def is_superset(self, other: "Group") -> bool:
		assert isinstance(other, Group)
		if self.pre == "" and other.pre != "":
			return False
		if self.post == "" and other.post != "":
			return False
		return other.pre.startswith(self.pre) and other.post.endswith(self.post)

	def is_partial(self) -> bool:
		return self.pre == "" or self.post == ""

	def is_empty(self) -> bool:
		return self.pre == "" and self.post == ""

	def gen_superset(self, other: "Group") -> "Group":
		assert isinstance(other, Group)
		pre = 0
		post = len(self.pre)
		for i in range(min(len(self.pre), len(other.pre))):
			if self.pre[i] == other.pre[i]:
				pre = i
			else:
				break
		for i in map(lambda x: -x - 1, range(min(len(self.post), len(other.post)))):
			if self.post[i] == other.post[i]:
				post = i
			else:
				break
		return Group(self.pre[:pre], self.post[post:])

	def reduce_numbers(self):
		pre = self.pre
		for i in range(len(self.pre)):
			if self.pre[-i - 1] in _numbers:
				pre = self.pre[:-i - 1]
			else:
				break
		post = self.post
		for i in range(len(self.post)):
			if self.post[i] in _numbers:
				post = self.post[i + 1:]
			else:
				break
		return Group(pre, post)

	def __repr__(self) -> str:
		return "Group(pre=%s, post=%s)" % (repr(self.pre), repr(self.post))


class File:
	file:str
	internal:str
	ending:str
	__matchers = (
		re.compile("^(?P<name>.*) ep[0-9]+$"), # Old episode format
		re.compile("^(?P<name>.*) S[0-9]+E[0-9]+$"), # New episode format with season
		re.compile("^(?P<name>.*)-[a-zA-Z0-9_-]{11}") # Youtube DL cleaner (have to last)
	)

	def __init__(self, file:str):
		# Save file
		assert isinstance(file, str)
		self.file = file

		# Remove postfix
		if file.lower().endswith(".webm"):
			self.internal = file[:-5]
			self.ending = file[-5:].lower()
		elif file.lower().endswith((".mp4", ".mkv")):
			self.internal = file[:-4]
			self.ending = file[-4:].lower()
		else:
			raise ArithmeticError("Unknown file type of '%s'." % file)

		# Remove episode info
		for matcher in self.__matchers:
			tmp = matcher.match(self.internal)
			if tmp:
				self.internal = tmp.groupdict()["name"]
			assert type(self.internal) == str

	def genEpisodeName(self, episode:int, season:int = 1) -> str:
		return self.internal + " S%iE%i" % (season, episode)

	def renameEpisode(self, episode:int, path:str):
		# Rename file
		old = os.path.join(path, self.file)
		try:
			new = os.path.join(path, self.genEpisodeName(episode) + self.ending.lower())
		except Exception:
			raise ValueError("%s %s %s" % (repr(path), repr(self.genEpisodeName(episode)), repr(self.ending.lower())))
		if old != new:
			print("Rename: %s --> %s" % (repr(old), repr(new)))
			os.rename(old, new)


class GroupFiles:
	groups:typing.List[Group]
	files:typing.Dict[str, File]

	__heuristics:typing.List[re.Pattern] = [
		re.compile("^.*#( )*(?P<id>[0-9]+)([^0-9].*)?$"),
	]

	def __init__(self, files:typing.List[typing.Union[File, str]]):
		tmp_files : typing.List[File] = tuple(map(lambda x: x if isinstance(x, File) else File(x), files))
		tmp_names : typing.List[str] = tuple(map(lambda x: x.file, tmp_files))
		self.files = dict(zip(tmp_names, tmp_files))
		groups : typing.List[Group] = []

		# Generate groups
		not_matched:typing.Set[File] = set()
		partial_matches:typing.Set[File] = set()
		for i in tmp_files:
			# Check if fully matched
			found = False
			for j in filter(lambda x: not x.is_partial(), groups):
				if j.match(i):
					found = True
					break
			if not found:
				for j in filter(lambda x: not x.is_partial(), groups):
					if j.match(i):
						found = True
						partial_matches.add(i)
						break
				if not found:
					for j in not_matched:
						tmp = Group.makeGroupe(i.internal, j.internal)
						if not tmp.is_empty():
							not_matched.remove(j)
							if tmp.is_partial():
								partial_matches.add(i)
								partial_matches.add(j)
							groups.append(tmp)
							break
					if not found:
						not_matched.add(i)

		# Try to generate completes from partials
		for i, j in itertools.combinations(itertools.chain(not_matched, partial_matches), 2):
			if i != j:
				tmp = Group.makeGroupe(i.internal, j.internal)
				if not tmp.is_partial():
					groups.append(tmp)

		# Reduction and ordering
		groups_partial:typing.Set[Group] = set()
		groups_complet:typing.Set[Group] = set()
		for i in groups:
			if not i.is_partial():
				to_add = True
				for j in filter(lambda x: x.is_superset(i), groups_complet):
					to_add = False
					break
				for j in list(filter(lambda x: i.is_superset(x), groups_complet)):
					to_add = True
					groups_complet.remove(j)
				if to_add:
					groups_complet.add(i)
			else:
				to_add = True
				for j in filter(lambda x: x.is_superset(i), groups_partial):
					to_add = False
					break
				for j in list(filter(lambda x: i.is_superset(x), groups_partial)):
					to_add = True
					groups_partial.remove(j)
				if to_add:
					groups_partial.add(i)

		# Reduction of multiple endings with same start
		same_start:typing.Dict[str, typing.Set[Group]] = {}
		same_end:typing.Dict[str, typing.Set[Group]] = {}
		for i in groups_complet:
			if i.pre in same_start.keys():
				same_start[i.pre].add(i)
			else:
				same_start[i.pre] = {i}
			if i.post in same_end.keys():
				same_end[i.post].add(i)
			else:
				same_end[i.post] = {i}
		reduced_groups:typing.List[Group] = list()
		for isStart, iID, i in sorted(itertools.chain(
			map(lambda x: (True, x[0], x[1]), same_start.items()),
			map(lambda x: (False, x[0], x[1]), same_end.items())
		), key=lambda x: len(x[2]), reverse=True):
			if len(i) > 1:
				if isStart:
					reduced_groups.append(Group(iID, ""))
				else:
					reduced_groups.append(Group("", iID))
			else:
				reduced_groups.append(next(iter(i)))

		# Merge reduced
		groups = list(map(Group.reduce_numbers, itertools.chain(reduced_groups, groups_partial)))
		if not_matched:
			groups.append(Group.makeGroupe("", ""))
		self.groups = groups

	def getName(self, file:str) -> str:
		assert isinstance(file, str)
		for i in self.groups:
			if i.match(self.files[file].internal):
				return i.getName(self.files[file])
		raise ValueError("Can't match file %s" % repr(file))

	def getInfo(self, file:str) -> typing.Tuple[int, str]:
		assert isinstance(file, str)
		name = self.getName(file)
		counter: int
		if not name:
			print("Can't find name: " + repr(name))
			return ((), name)
		else:
			# heuristic
			i:typing.Union[None, re.Match]
			for i in map(lambda x: x.match(name), self.__heuristics):
				if i is not None:
					counter = int(i.groupdict()["id"])
					return counter, name

			if name[0] in _numbers:
				counter = 1
				while len(name) > counter and name[counter] in _numbers:
					counter += 1
				counter = float(name[:counter]) if "." in name[:counter] else int(name[:counter])
			elif name[-1] in _numbers:
				counter = 1
				while len(name) > counter and name[-counter - 1] in _numbers:
					counter += 1
				counter = int(name[-counter:])
			else:
				# Can't solve
				raise ValueError("Can't parse file %s." % repr(name))
		return (counter, name)

	def __str__(self) -> str:
		return "<GroupFiles %s>" % ", ".join(map(str, self.groups))
