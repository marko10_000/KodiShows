# SPDX-License-Identifier: AGPL-3.0
import argparse
import os
import typing
import re
import warnings
from . import groups, infofile, utils


__formats = (".mp4", ".webm", ".mkv")


def loadFiles(dirs:typing.List[str], filters:typing.List[str]) -> typing.List[groups.File]:
	assert isinstance(dirs, list)
	assert isinstance(filters, list)
	# Load filters
	compt_filters = []
	for i in filters:
		assert isinstance(i, str)
		compt_filters.append(re.compile(i))

	# Load files
	result = []
	for i in dirs:
		assert isinstance(i, str)
		if os.path.isdir(i):
			for j in os.listdir(i):
				if j.endswith(__formats):
					to_continue = not bool(compt_filters)
					tmp = os.path.basename(j)
					for cf in compt_filters:
						if cf.match(tmp):
							to_continue = True
							break
					if to_continue:
						result.append(groups.File(j))
		else:
			warnings.warn("Directory %s does not exists." % repr(i))
	return result


def main():
	import sys

	# Parse
	parser = argparse.ArgumentParser()
	parser.add_argument("--add-info", "-a", action="store_true", help="Enable to add info files.")
	parser.add_argument("--filter", "-f", action="append", type=str, default=[], help="Regex filter for files to include.")
	parser.add_argument("target", nargs=1, type=str, help="Output directory. Is an input too.")
	parser.add_argument("sources", nargs="*", type=str, default=[], help="Sources to load files from.")
	args = parser.parse_args()

	# Load files
	gs = groups.GroupFiles(loadFiles(args.sources + args.target, args.filter))

	# Order files
	order = {}
	for i in gs.files.keys():
		index, name = gs.getInfo(i)
		if index == ():
			print("For file %s no episode number found." % repr(i))
			exit(2)
		elif index in order:
			print("File %s has episode number %s with file %s" % (repr(i), repr(index), repr(order[index][1])))
			exit(3)
		order[index] = (name, i)
	order = sorted(order.items(), key=lambda x: x[0])

	# Move files
	for iID, (_, (_, file)) in enumerate(order, 1):
		try:
			gs.files[file].renameEpisode(iID, args.target[0])
		except Exception:
			raise RuntimeError("Problems with: " + repr(file))
	if args.add_info:
		utils.updateFile(os.path.join(args.target[0], "tvshow.nfo"), infofile.genShow(os.path.basename(args.target[0])).encode())
		for iID, (_, (_, file)) in enumerate(order, 1):
			file_nfo = os.path.join(args.target[0], gs.files[file].genEpisodeName(iID) + ".nfo")
			utils.updateFile(file_nfo, infofile.genEpisode(1, iID, gs.getName(file)).encode())


if __name__ == "__main__":
	main()
