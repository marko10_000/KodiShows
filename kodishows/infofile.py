# SPDX-License-Identifier: AGPL-3.0
import hashlib
from xml.dom import minidom


def genEpisode(season:int, episode:int, name:str) -> str:
	assert(isinstance(season, int))
	assert(isinstance(episode, int))
	assert(isinstance(name, str))
	doc = minidom.Document()
	desc = doc.createElement("episodedetails")
	doc.appendChild(desc)
	tmp = doc.createElement("season")
	tmp.appendChild(doc.createTextNode("%i" % season))
	desc.appendChild(tmp)
	tmp = doc.createElement("title")
	tmp.appendChild(doc.createTextNode(name))
	desc.appendChild(tmp)
	return doc.toprettyxml()


def genShow(name:str) -> str:
	assert(isinstance(name, str))
	doc = minidom.Document()
	desc = doc.createElement("tvshow")
	doc.appendChild(desc)
	tmp = doc.createElement("id")
	tmp2 = hashlib.sha1()
	tmp2.update(name.encode())
	tmp.appendChild(doc.createTextNode(tmp2.hexdigest()))
	desc.appendChild(tmp)
	tmp = doc.createElement("title")
	tmp.appendChild(doc.createTextNode(name))
	desc.appendChild(tmp)
	return doc.toprettyxml()
