import itertools
from kodishows import groups
import unittest


class TestFile(unittest.TestCase):
	def test_youtube(self):
		f:groups.File = groups.File("Let's Play Prey #2 - Prey 2017 Gameplay German PC (1440p _ 60fps)-mKgwKQQk42M.mkv")
		self.assertEqual(f.ending, ".mkv")
		self.assertEqual(f.file, "Let's Play Prey #2 - Prey 2017 Gameplay German PC (1440p _ 60fps)-mKgwKQQk42M.mkv")
		self.assertEqual(f.internal, "Let's Play Prey #2 - Prey 2017 Gameplay German PC (1440p _ 60fps)")


class TestDataSets(unittest.TestCase):
	data_set1 = ("Let's Play Prey #2 - Prey 2017 Gameplay German PC (1440p _ 60fps)-mKgwKQQk42M.mkv",)
	data_set2 = (
		'Prey #5 - Hardware schon gesagt, wie hübsch das ist - (PC 1440p _ 60fps)-3Fp01_p2hd4.mkv',
		'Prey #6 - Gestatten, Thorstein - (PC 1440p _ 60fps)-vUtH-a6P8GQ.mkv'
	)
	data_set3 = (
		"TEST #101 ABC.mkv",
		"TEST #102 DEF.mkv"
	)

	def test_set1(self):
		loaded = groups.GroupFiles(self.data_set1)
		tmp, _ = loaded.getInfo(self.data_set1[0])
		self.assertEqual(tmp, 2)

	def test_set2(self):
		loaded = groups.GroupFiles(self.data_set2)
		tmp1, tmp2 = loaded.getInfo(self.data_set2[0])
		self.assertEqual(tmp1, 5)
		self.assertEqual(tmp2, "5 - Hardware schon gesagt, wie hübsch das ist")
		tmp1, tmp2 = loaded.getInfo(self.data_set2[1])
		self.assertEqual(tmp1, 6)
		self.assertEqual(tmp2, "6 - Gestatten, Thorstein")

	def test_set3(self):
		loaded = groups.GroupFiles(self.data_set3)
		tmp1, tmp2 = loaded.getInfo(self.data_set3[0])
		self.assertEqual(tmp1, 101)
		self.assertEqual(tmp2, "101 ABC")
		tmp1, tmp2 = loaded.getInfo(self.data_set3[1])
		self.assertEqual(tmp1, 102)
		self.assertEqual(tmp2, "102 DEF")

	def test_combined(self):
		for i in itertools.combinations((self.data_set1, self.data_set2, self.data_set3), 3):
			try:
				loaded = groups.GroupFiles(itertools.chain.from_iterable(i))
				tmp, _ = loaded.getInfo(self.data_set1[0])
				self.assertEqual(tmp, 2)
				tmp1, tmp2 = loaded.getInfo(self.data_set2[0])
				self.assertEqual(tmp1, 5)
				self.assertEqual(tmp2, "5 - Hardware schon gesagt, wie hübsch das ist")
				tmp1, tmp2 = loaded.getInfo(self.data_set2[1])
				self.assertEqual(tmp1, 6)
				self.assertEqual(tmp2, "6 - Gestatten, Thorstein")
			except:
				raise ValueError("Combination %r\n%r" % (i, loaded.groups))
